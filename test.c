#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "list.h"

typedef struct {
    list_item_t base;
    int real, imag;
} complex_t;

complex_t *
complex_new(int real, int imag)
{
    complex_t *this = malloc(sizeof(*this));

    if (this == NULL)
        return this;

    list_item_init(&this->base, &free, false);

    this->real = real;
    this->imag = imag;

    return this;
}

void
complex_print(complex_t *this)
{
    printf("<complex %d+%dj>", this->real, this->imag);
}

int
main(void)
{

    list_t *myList = list_new();

    // demonstrate retained items
    list_item_t *dontDeleteMe = (list_item_t *) complex_new(12, 34);
    list_item_setRetain(dontDeleteMe, true);
    list_add(myList, dontDeleteMe);

    // demonstrate removing items
    list_item_t *removeMe = (list_item_t *) complex_new(56, 78);
    list_add(myList, removeMe);
    list_remove(myList, removeMe);

    // populate the list
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            complex_t *c = complex_new(i, j);
            printf("adding ");
            complex_print(c);
            printf("\n");
            list_add(myList, (list_item_t *) c);
        }
    }

    // print the list's size
    printf("List size: %zu\n", myList->size);

    // print the list
    list_item_t *item = myList->head;
    while (item != NULL) {
        printf("item: ");
        complex_print((complex_t *) item);
        printf("\n");
        item = item->next;
    }

    // destroy the list
    list_destroy(myList);

    // demonstrate retained items
    printf("dontDeleteMe: ");
    complex_print((complex_t *) dontDeleteMe);
    printf("\n");

    // cleanup
    free(dontDeleteMe);

    return 0;
}
