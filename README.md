# list
A basic linked list written in C
---
To create a list:
```c
list_t *people = list_new();
```
To destroy a list:
```c
list_destroy(people);
```
To add items to a list:
```c
// note: this will work without casting to (item_t *)
item_t *bob = (item_t *) person_new("Bob");
list_add(people, bob);
```
To remove items from a list:
```c
list_remove(people, bob);
```
By default ```bob``` will be destroyed. Retained items are a way of preventing this:
```c
item_t *bob = ...;
item_setRetain(bob, true);
list_remove(people, bob);
// bob will not be destroyed, so you must do so manually
printf("%s\n", bob->name);
person_destroy(bob);
```
```item_init()``` is used to initialise an item in its constructor
```c
person_t *person = ...;
// use person_destroy to destroy person_t items, don't retain them
item_init(person, &person_destroy, false);
...
return person;
```
