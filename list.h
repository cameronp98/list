#include <stdbool.h>
#include <stddef.h>

/**
 * @TODO implement list iteration
 */

typedef struct list_item_t list_item_t;

/**
 * Represents a function used to destroy an item
 */
typedef void (destructor_t)(void *);

/**
 * The header member for a doubly-linked list item struct
 */
struct list_item_t {
	struct list_item_t *next, *prev;
	destructor_t *destroy;
	bool retain;
};

/**
 * Represents a doubly-linked list
 */
typedef struct {
	list_item_t *head, *tail;
	list_item_t **next;
	size_t size;
} list_t;

/**
 * Initialise an item
 *
 * @param this The item
 * @param destroy The item's destructor
 * @param retain The item's retain flag
 */
void
list_item_init(list_item_t *this, destructor_t *destroy, bool retain);

/**
 * Set whether an item should be retained
 *
 * @param this The item
 * @param retain The item's new retain flag
 */
void
list_item_setRetain(list_item_t *this, bool retain);

/**
 * Destroy an item if is not retained
 *
 * @param this The item
 */
bool
list_item_destroy(list_item_t *item);

/**
 * Return the next item in a list
 *
 * @param this The item
 */
list_item_t *
list_item_getNext(list_item_t *this);

/**
 * Return the next item in a list
 *
 * @param this The item
 */
list_item_t *
list_item_getPrevious(list_item_t *this);

/**
 * Create a new list
 *
 * @return The new list
 */
list_t *
list_new(void);

/**
 * Initialise a list
 *
 * @param this The list
 */
void
list_init(list_t *this);

/**
 * Remove all elements from a list
 *
 * @param this The list
 */
void
list_empty(list_t *this);

/**
 * Destroy a list
 *
 * @param this The list
 */
void
list_destroy(list_t *this);

/**
 * Add an item to a list
 *
 * @param this The list
 * @param item The item
 */
void
list_add(list_t *this, list_item_t *item);

/**
 * Add an item to the start of a list
 *
 * @param this The list
 * @param item The item
 */
void
list_addFirst(list_t *this, list_item_t *item);

/**
 * Remove an item from a list
 *
 * @param this The list
 * @param item The item
 */
void
list_remove(list_t *this, list_item_t *item);

/**
 * Check if a list is empty
 *
 * @param this The list
 */
bool
list_isEmpty(list_t *this);

/**
 * Get the next item from a list
 *
 * @param this The list
 */
list_item_t *
list_getNext(list_t *this);

/**
 * Reset list iteration
 *
 * @param this The list
 */
void
list_reset(list_t *this);

/**
 * Get the first item from a list
 *
 * @param this The list
 */
list_item_t *
list_getFirst(list_t *this);

/**
 * Get the last item from a list
 *
 * @param this The list
 */
list_item_t *
list_getLast(list_t *this);
