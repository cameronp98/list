#include "list.h"

#include <stdlib.h>

void
list_item_init(list_item_t *this, destructor_t *destroy, bool retain)
{
    this->next = NULL;
    this->prev = NULL;
    this->destroy = destroy;
    this->retain = retain;
}

void
list_item_setRetain(list_item_t *this, bool retain)
{
    this->retain = retain;
}

bool
list_item_destroy(list_item_t *item)
{
    if (!item->retain) {
        item->destroy(item);
        return true;
    }
    return false;
}

list_item_t *
list_item_getNext(list_item_t *this)
{
	return this->next;
}

list_item_t *
list_item_getPrevious(list_item_t *this)
{
	return this->prev;
}

list_t *
list_new(void)
{
    list_t *this = malloc(sizeof(*this));

    if (this == NULL)
        return NULL;

    list_init(this);

    return this;
}

void
list_init(list_t *this)
{
    this->head = NULL;
    this->tail = NULL;
    this->next = &this->head;
    this->size = 0;
}

void
list_empty(list_t *this)
{
    list_item_t *item = this->head;
    list_item_t *next;

    while (item != NULL) {
        next = item->next;
        list_item_destroy(item);
        item = next;
    }

    list_init(this);
}

void
list_destroy(list_t *this)
{
    list_empty(this);
    free(this);
}

void
list_add(list_t *this, list_item_t *item)
{
    if (list_isEmpty(this)) {
        this->head = item;
        this->tail = item;
    } else {
        this->tail->next = item;
        item->prev = this->tail;
    }

    this->tail = item;
    this->size++;
}

void
list_addFirst(list_t *this, list_item_t *item)
{
    if (list_isEmpty(this)) {
        this->tail = item;
    } else {
        this->head->prev = item;
        item->next = this->head;
    }

    this->head = item;
    this->size++;
}

void
list_remove(list_t *this, list_item_t *item)
{
    // try and unlink the item from the list
    if (item->prev != NULL)
        item->prev->next = item->next;

    if (item->next != NULL)
        item->next->prev = item->prev;

    // relink the list's head and tail if necessary
    if (this->head == item)
        this->head = item->next;

    if (this->tail == item)
        this->tail = item->prev;

    // try and destroy the item
    list_item_destroy(item);

    this->size--;
}

bool
list_isEmpty(list_t *this)
{
    return (this->size == 0);
}

list_item_t *
list_getNext(list_t *this)
{
    if (*this->next == NULL)
        return NULL;

    list_item_t *item = *this->next;

    this->next = &item->next;

    return item;
}

void
list_reset(list_t *this)
{
    this->next = &this->head;
}

list_item_t *
list_getFirst(list_t *this)
{
    return this->head;
}

list_item_t *
list_getLast(list_t *this)
{
    return this->tail;
}
